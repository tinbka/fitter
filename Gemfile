source 'https://rubygems.org'
ruby '2.2.3'

gem 'rails', '~> 4.2'

### Models ###

# Adapters
gem 'pg', groups: [:development, :test]
gem 'tiny_tds'
gem 'activerecord-sqlserver-adapter'
# CMS
gem 'activeadmin', '~> 1.0.0.pre2'
# Pagination
gem 'kaminari'
# Nesting
gem 'ancestry'


### Middleware ###

# Web-servers
gem 'puma'
# Auth
gem 'devise'
gem 'omniauth'
gem 'omniauth-fitbit'
gem 'omniauth-underarmour'
gem 'cancancan'


### Presentation ###

# Templating
gem 'slim'
# Static
gem 'high_voltage'
# Decoration
gem 'draper'


### Assets ###

# JS runtime
gem 'therubyracer'
# Preprocessors
gem 'sass-rails'
gem 'coffee-rails'
# Postprocessors
gem 'autoprefixer-rails'
gem 'uglifier'
# CSS libs
gem 'bootstrap-sass'
gem 'font-awesome-sass'
# JS libs
gem 'jquery-rails'
gem 'turbolinks' # one-page helper
# Package managers
gem 'bower'


### Web clients ###

gem 'typhoeus'


### Other ###

# Utils
gem 'config' # common and per-developer settings
gem 'dotenv-rails' # load environment variables from `.env`
gem 'tracee', '1.0.0' # logger and stacktrace decorator


# group :test do
#   # BDD
#   gem 'rspec-rails'
#   # Helpers
#   gem 'database_cleaner'
#   gem 'factory_girl_rails' # factories
#   gem 'timecop' # DateTime mocks
# end

group :development do
  # Deployment
  gem 'capistrano', '~> 3.4.0', require: false
  gem 'capistrano-rails', '~> 1.1', require: false
  gem 'capistrano-rvm', require: false
  gem 'capistrano-bundler', require: false
  gem 'capistrano3-puma', require: false
  gem 'capistrano-sidekiq', require: false
  gem 'capistrano-shoryuken', require: false
  gem 'airbrussh', require: false
  gem 'capistrano-git-copy', require: false
  
  # Console common
  gem 'awesome_print'     # presents nested objects with coloring
  gem 'hirb'              # presents 2D-objects as ascii tables. With pager
  gem 'method_source'     # finds classes/methods definitions/comments
  gem 'looksee'           # illustrating the ancestry and method lookup path of objects
  gem 'coderay'           # highlights syntax as html or ansi
  gem 'alias'             # create shortcuts for your favorite methods
  gem 'byebug'            # debugger
  
  # Entity relationship diagram generator
  gem 'rails-erd'
end

group :development, :staging do
  # Error page
  gem 'better_errors'
  gem 'binding_of_caller'
end
