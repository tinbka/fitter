class CreateFitbitDevices < ActiveRecord::Migration
  def change
    create_table :fitbit_devices, id: false do |t|
      t.string :id, null: false, index: true, unique: true # value from fitbit
      
      t.references :user, index: true
      t.foreign_key :fitbit_users, column: :user_id
      
      t.string :battery
      t.string :deviceVersion
      t.datetime :lastSyncTime
      t.string :mac
      t.string :type
      t.string :features, null: false, default: '[]'
    end
  end
end
