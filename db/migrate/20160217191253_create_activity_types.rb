class CreateActivityTypes < ActiveRecord::Migration
  def change
    create_table :fitbit_activity_categories, id: false do |t|
      t.integer :id, null: true # must not be auto-incremental
      t.string :ancestry
      t.string :name
    end
    add_index :fitbit_activity_categories, :id, unique: true
    add_index :fitbit_activity_categories, :ancestry
    
    create_table :fitbit_activity_types, id: false do |t|
      t.integer :id, null: true # must not be auto-incremental
      t.integer :activity_category_id, null: true
      t.string :name
      t.string :accessLevel
      t.boolean :hasSpeed
      t.decimal :mets, precision: 3, scale: 1
      t.text :activityLevels, null: false, default: '[]'
    end
    add_index :fitbit_activity_types, :id
    add_index :fitbit_activity_types, :activity_category_id
  end
end
