class AddFoodGoalsToFitbitUsers < ActiveRecord::Migration
  def change
    add_column :fitbit_users, :food_goals, :string, null: false, default: '{}'
    add_column :fitbit_users, :food_plan, :string, null: false, default: '{}'
  end
end
