class CreateUnderarmourActigraphies < ActiveRecord::Migration
  def change
    create_table :underarmour_actigraphies do |t|
      t.string :user_id, index: true
      t.foreign_key :underarmour_users, column: :user_id
      t.date :date
      t.index [:user_id, :date]
      
      t.text :aggregates, null: false, default: '{}'
      t.text :metrics, null: false, default: '{}'
      t.text :workouts, null: false, default: '[]'
      t.string :timezones, null: false, default: '[]'
      
      t.datetime :start_datetime_utc
      t.datetime :end_datetime_utc
    end
  end
end
