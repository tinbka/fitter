class CreateFitbitWeightLogs < ActiveRecord::Migration
  def change
    create_table :fitbit_weight_logs, id: false do |t|
      t.references :user
      t.decimal :bmi, precision: 4, scale: 2
      t.date :date
      t.integer :logId, null: false # primary key for deletion
      t.string :time
      t.integer :weight
      t.string :source
    end
    add_index :fitbit_weight_logs, :user_id
    add_index :fitbit_weight_logs, :logId
  end
end
