class CreateUnderarmourDataSources < ActiveRecord::Migration
  def change
    create_table :underarmour_data_sources do |t|
      t.string :user_id, index: true
      t.foreign_key :underarmour_users, column: :user_id
      
      t.string :name
      t.boolean :active
      t.string :advertised_name
      t.string :serial_number
      t.text :_embedded, null: false, default: '{}' # {devices: <devices list>}
    end
  end
end
