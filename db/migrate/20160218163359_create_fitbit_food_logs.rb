class CreateFitbitFoodLogs < ActiveRecord::Migration
  def change
    create_table :fitbit_food_summaries do |t|
      t.references :user, index: true
      t.foreign_key :fitbit_users, column: :user_id
      t.date :date, index: true
      t.index [:user_id, :date]
      
      t.text :water, null: false, default: '[]'
      t.text :foods, null: false, default: '[]'
      t.text :summary, null: false, default: '{}'
      t.string :goals, null: false, default: '{}'
    end
  end
end
