class CreateFitbitActivitySummaries < ActiveRecord::Migration
  def change
    # Aggregation table
    create_table :fitbit_activity_summaries do |t|
      t.integer :user_id, null: false
      t.date :date, null: false
      t.string :goals, null: false # json
      t.text :summary, null: false # json
    end
    
    add_index :fitbit_activity_summaries, :user_id
    add_index :fitbit_activity_summaries, :date
    add_index :fitbit_activity_summaries, [:user_id, :date]
  end
end
