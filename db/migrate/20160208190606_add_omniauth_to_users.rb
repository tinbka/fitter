class AddOmniauthToUsers < ActiveRecord::Migration
  def change
    add_column :users, :provider, :string
    add_column :users, :uid, :string
    
    [:fitbit, :underarmour].each do |provider|
      add_column :users, :"#{provider}_token", :string
      add_column :users, :"#{provider}_refresh_token", :string
      add_column :users, :"#{provider}_token_expires_at", :datetime
    end
  end
end
