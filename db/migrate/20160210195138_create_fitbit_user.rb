class CreateFitbitUser < ActiveRecord::Migration
  def change
    create_table :fitbit_users do |t|
      t.integer :age
      t.string :avatar
      t.string :avatar150
      t.integer :averageDailySteps
      t.date :dateOfBirth
      t.string :displayName
      t.string :distanceUnit
      t.string :encodedId
      t.string :features # json
      t.string :foodsLocale
      t.string :fullName
      t.string :gender
      t.string :glucoseUnit
      t.decimal :height, precision: 6, scale: 2
      t.string :heightUnit
      t.string :locale
      t.date :memberSince
      t.integer :offsetFromUTCMillis
      t.string :startDayOfWeek
      t.decimal :strideLengthRunning, precision: 8, scale: 2
      t.decimal :strideLengthWalking, precision: 8, scale: 2
      t.string :timezone
      t.string :waterUnit
      t.string :waterUnitName
      t.decimal :weight, precision: 6, scale: 2
      t.string :weightUnit
    end
    
    add_index :fitbit_users, :encodedId, unique: true
  end
end
