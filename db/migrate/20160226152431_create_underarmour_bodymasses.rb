class CreateUnderarmourBodymasses < ActiveRecord::Migration
  def change
    create_table :underarmour_bodymasses, id: false do |t|
      t.string :user_id, index: true
      t.foreign_key :underarmour_users, column: :user_id
      t.integer :external_id, index: {unique: true} # id from ua.com
      
      t.decimal :fat_mass, precision: 6, scale: 3
      t.decimal :lean_mass, precision: 6, scale: 3
      t.decimal :mass, precision: 6, scale: 3
      t.decimal :bmi, precision: 4, scale: 2
      t.decimal :fat_percent, precision: 4, scale: 2
      
      t.string :datetime_timezone
      t.datetime :datetime_utc
      t.datetime :created_datetime
      t.datetime :updated_datetime
    end
  end
end
