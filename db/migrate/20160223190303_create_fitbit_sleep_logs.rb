class CreateFitbitSleepLogs < ActiveRecord::Migration
  def change
    create_table :fitbit_sleep_summaries do |t|
      t.references :user, index: true
      t.foreign_key :fitbit_users, column: :user_id
      t.date :date
      t.index [:user_id, :date]
      
      t.text :sleep, null: false, default: '[]'
      t.string :summary, null: false, default: '{}'
    end
  end
end
