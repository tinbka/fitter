class CreateHeartActivities < ActiveRecord::Migration
  def change
    create_table :fitbit_heart_activities do |t|
      t.references :user, index: true
      t.foreign_key :fitbit_users, column: :user_id
      t.date :date
      t.index [:user_id, :date]
      
      t.text :value, null: false, default: '{}'
    end
  end
end
