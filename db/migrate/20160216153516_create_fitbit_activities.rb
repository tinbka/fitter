class CreateFitbitActivities < ActiveRecord::Migration
  def change
    create_table :fitbit_activities do |t|
      t.integer :user_id, null: false
      t.integer :activityId
      t.integer :activityParentId
      t.string :activityParentName
      t.integer :calories
      t.string :description
      t.integer :distance
      t.integer :duration
      t.boolean :hasStartTime
      t.boolean :isFavorite
      t.datetime :lastModified
      t.integer :logId
      t.string :name
      t.date :startDate, null: false
      t.string :startTime
      t.integer :steps
    end
    
    add_index :fitbit_activities, :user_id
    add_index :fitbit_activities, :startDate
    add_index :fitbit_activities, [:user_id, :startDate]
  end
end
