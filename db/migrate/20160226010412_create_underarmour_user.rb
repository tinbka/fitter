class CreateUnderarmourUser < ActiveRecord::Migration
  def change
    create_table :underarmour_users, id: false do |t|
      t.string :id, null: false, index: {unique: true} # id key from ua.com
      t.string :email, index: {unique: true}
      
      t.string :username
      t.string :first_name
      t.string :last_name
      t.string :last_initial
      t.string :display_name
      t.string :gender
      t.string :preferred_language
      t.date :birthdate
      t.string :goal_statement
      t.string :profile_statement
      t.string :hobbies
      t.string :introduction
      t.string :display_measurement_system
      t.string :time_zone
      t.decimal :height, precision: 5, scale: 2
      t.decimal :weight, precision: 5, scale: 2
      t.datetime :date_joined
      t.datetime :last_login # equals to date_joined at the moment
      
      t.string :communication, null: false, default: '{}'
      t.string :location, null: false, default: '{}'
      t.string :sharing, null: false, default: '{}'
      t.text :_links, null: false, default: '{}'
    end
  end
end
