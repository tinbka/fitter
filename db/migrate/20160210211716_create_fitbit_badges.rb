class CreateFitbitBadges < ActiveRecord::Migration
  def change
    create_table :fitbit_badges do |t|
      t.string :badgeGradientEndColor
      t.string :badgeGradientStartColor
      t.string :badgeType
      t.string :category
      t.string :cheers, null: false, default: '[]' # json array
      t.date :dateTime
      t.string :description
      t.string :earnedMessage
      t.string :encodedId, null: false
      t.string :image100px
      t.string :image125px
      t.string :image300px
      t.string :image50px
      t.string :image75px
      t.string :marketingDescription
      t.string :mobileDescription
      t.string :name
      t.string :shareImage640px
      t.string :shareText
      t.string :shortDescription
      t.string :shortName
      t.integer :timesAchieved
      t.string :unit
      t.integer :value
    end
    
    # add_index :fitbit_badges, :encodedId, unique: true
    
    create_table :fitbit_badges_fitbit_users, id: false do |t|
      t.integer :badge_id, null: false
      t.integer :user_id, null: false
    end
    
    add_index :fitbit_badges_fitbit_users, :user_id
    add_index :fitbit_badges_fitbit_users, :badge_id
    add_index :fitbit_badges_fitbit_users, [:badge_id, :user_id], unique: true
  end
end
