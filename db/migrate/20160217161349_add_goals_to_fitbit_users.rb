class AddGoalsToFitbitUsers < ActiveRecord::Migration
  def change
    add_column :fitbit_users, :daily_goals, :string, null: false, default: '{}'
    add_column :fitbit_users, :weekly_goals, :string, null: false, default: '{}'
  end
end
