class CreateUnderarmourDevices < ActiveRecord::Migration
  def change
    create_table :underarmour_devices do |t|
      t.string :name
      t.string :description
      t.string :manufacturer
      t.string :model, null: false, index: {unique: true}
    end
  end
end
