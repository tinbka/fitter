class AddBodyGoalsToFitbitUsers < ActiveRecord::Migration
  def change
    add_column :fitbit_users, :fat_goal, :string, null: false, default: '{}'
    add_column :fitbit_users, :weight_goal, :string, null: false, default: '{}'
  end
end
