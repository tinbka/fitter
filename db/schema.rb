# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20160227002341) do

  create_table "active_admin_comments", force: :cascade do |t|
    t.string   "namespace",     limit: 4000
    t.text     "body",          limit: 2147483647
    t.string   "resource_id",   limit: 4000,       null: false
    t.string   "resource_type", limit: 4000,       null: false
    t.integer  "author_id",     limit: 4
    t.string   "author_type",   limit: 4000
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "active_admin_comments", ["author_type", "author_id"], name: "index_active_admin_comments_on_author_type_and_author_id"
  add_index "active_admin_comments", ["namespace"], name: "index_active_admin_comments_on_namespace"
  add_index "active_admin_comments", ["resource_type", "resource_id"], name: "index_active_admin_comments_on_resource_type_and_resource_id"

  create_table "fitbit_activities", force: :cascade do |t|
    t.integer  "user_id",            limit: 4,    null: false
    t.integer  "activityId",         limit: 4
    t.integer  "activityParentId",   limit: 4
    t.string   "activityParentName", limit: 4000
    t.integer  "calories",           limit: 4
    t.string   "description",        limit: 4000
    t.integer  "distance",           limit: 4
    t.integer  "duration",           limit: 4
    t.boolean  "hasStartTime"
    t.boolean  "isFavorite"
    t.datetime "lastModified"
    t.integer  "logId",              limit: 4
    t.string   "name",               limit: 4000
    t.date     "startDate",                       null: false
    t.string   "startTime",          limit: 4000
    t.integer  "steps",              limit: 4
  end

  add_index "fitbit_activities", ["startDate"], name: "index_fitbit_activities_on_startDate"
  add_index "fitbit_activities", ["user_id", "startDate"], name: "index_fitbit_activities_on_user_id_and_startDate"
  add_index "fitbit_activities", ["user_id"], name: "index_fitbit_activities_on_user_id"

  create_table "fitbit_activity_categories", id: false, force: :cascade do |t|
    t.integer "id",       limit: 4
    t.string  "ancestry", limit: 4000
    t.string  "name",     limit: 4000
  end

  add_index "fitbit_activity_categories", ["ancestry"], name: "index_fitbit_activity_categories_on_ancestry"
  add_index "fitbit_activity_categories", ["id"], name: "index_fitbit_activity_categories_on_id", unique: true

  create_table "fitbit_activity_summaries", force: :cascade do |t|
    t.integer "user_id", limit: 4,          null: false
    t.date    "date",                       null: false
    t.string  "goals",   limit: 4000,       null: false
    t.text    "summary", limit: 2147483647, null: false
  end

  add_index "fitbit_activity_summaries", ["date"], name: "index_fitbit_activity_summaries_on_date"
  add_index "fitbit_activity_summaries", ["user_id", "date"], name: "index_fitbit_activity_summaries_on_user_id_and_date"
  add_index "fitbit_activity_summaries", ["user_id"], name: "index_fitbit_activity_summaries_on_user_id"

  create_table "fitbit_activity_types", id: false, force: :cascade do |t|
    t.integer "id",                   limit: 4
    t.integer "activity_category_id", limit: 4
    t.string  "name",                 limit: 4000
    t.string  "accessLevel",          limit: 4000
    t.boolean "hasSpeed"
    t.decimal "mets",                                    precision: 3, scale: 1
    t.text    "activityLevels",       limit: 2147483647,                         default: "[]", null: false
  end

  add_index "fitbit_activity_types", ["activity_category_id"], name: "index_fitbit_activity_types_on_activity_category_id"
  add_index "fitbit_activity_types", ["id"], name: "index_fitbit_activity_types_on_id"

  create_table "fitbit_badges", force: :cascade do |t|
    t.string  "badgeGradientEndColor",   limit: 4000
    t.string  "badgeGradientStartColor", limit: 4000
    t.string  "badgeType",               limit: 4000
    t.string  "category",                limit: 4000
    t.string  "cheers",                  limit: 4000, default: "[]", null: false
    t.date    "dateTime"
    t.string  "description",             limit: 4000
    t.string  "earnedMessage",           limit: 4000
    t.string  "encodedId",               limit: 4000,                null: false
    t.string  "image100px",              limit: 4000
    t.string  "image125px",              limit: 4000
    t.string  "image300px",              limit: 4000
    t.string  "image50px",               limit: 4000
    t.string  "image75px",               limit: 4000
    t.string  "marketingDescription",    limit: 4000
    t.string  "mobileDescription",       limit: 4000
    t.string  "name",                    limit: 4000
    t.string  "shareImage640px",         limit: 4000
    t.string  "shareText",               limit: 4000
    t.string  "shortDescription",        limit: 4000
    t.string  "shortName",               limit: 4000
    t.integer "timesAchieved",           limit: 4
    t.string  "unit",                    limit: 4000
    t.integer "value",                   limit: 4
  end

  create_table "fitbit_badges_fitbit_users", id: false, force: :cascade do |t|
    t.integer "badge_id", limit: 4, null: false
    t.integer "user_id",  limit: 4, null: false
  end

  add_index "fitbit_badges_fitbit_users", ["badge_id", "user_id"], name: "index_fitbit_badges_fitbit_users_on_badge_id_and_user_id", unique: true
  add_index "fitbit_badges_fitbit_users", ["badge_id"], name: "index_fitbit_badges_fitbit_users_on_badge_id"
  add_index "fitbit_badges_fitbit_users", ["user_id"], name: "index_fitbit_badges_fitbit_users_on_user_id"

  create_table "fitbit_devices", id: false, force: :cascade do |t|
    t.string   "id",            limit: 4000,                null: false
    t.integer  "user_id",       limit: 4
    t.string   "battery",       limit: 4000
    t.string   "deviceVersion", limit: 4000
    t.datetime "lastSyncTime"
    t.string   "mac",           limit: 4000
    t.string   "type",          limit: 4000
    t.string   "features",      limit: 4000, default: "[]", null: false
  end

  add_index "fitbit_devices", ["id"], name: "index_fitbit_devices_on_id"
  add_index "fitbit_devices", ["user_id"], name: "index_fitbit_devices_on_user_id"

  create_table "fitbit_food_summaries", force: :cascade do |t|
    t.integer "user_id", limit: 4
    t.date    "date"
    t.text    "water",   limit: 2147483647, default: "[]", null: false
    t.text    "foods",   limit: 2147483647, default: "[]", null: false
    t.text    "summary", limit: 2147483647, default: "{}", null: false
    t.string  "goals",   limit: 4000,       default: "{}", null: false
  end

  add_index "fitbit_food_summaries", ["date"], name: "index_fitbit_food_summaries_on_date"
  add_index "fitbit_food_summaries", ["user_id", "date"], name: "index_fitbit_food_summaries_on_user_id_and_date"
  add_index "fitbit_food_summaries", ["user_id"], name: "index_fitbit_food_summaries_on_user_id"

  create_table "fitbit_heart_activities", force: :cascade do |t|
    t.integer "user_id", limit: 4
    t.date    "date"
    t.text    "value",   limit: 2147483647, default: "{}", null: false
  end

  add_index "fitbit_heart_activities", ["user_id", "date"], name: "index_fitbit_heart_activities_on_user_id_and_date"
  add_index "fitbit_heart_activities", ["user_id"], name: "index_fitbit_heart_activities_on_user_id"

  create_table "fitbit_sleep_summaries", force: :cascade do |t|
    t.integer "user_id", limit: 4
    t.date    "date"
    t.text    "sleep",   limit: 2147483647, default: "[]", null: false
    t.string  "summary", limit: 4000,       default: "{}", null: false
  end

  add_index "fitbit_sleep_summaries", ["user_id", "date"], name: "index_fitbit_sleep_summaries_on_user_id_and_date"
  add_index "fitbit_sleep_summaries", ["user_id"], name: "index_fitbit_sleep_summaries_on_user_id"

  create_table "fitbit_users", force: :cascade do |t|
    t.integer "age",                 limit: 4
    t.string  "avatar",              limit: 4000
    t.string  "avatar150",           limit: 4000
    t.integer "averageDailySteps",   limit: 4
    t.date    "dateOfBirth"
    t.string  "displayName",         limit: 4000
    t.string  "distanceUnit",        limit: 4000
    t.string  "encodedId",           limit: 4000
    t.string  "features",            limit: 4000
    t.string  "foodsLocale",         limit: 4000
    t.string  "fullName",            limit: 4000
    t.string  "gender",              limit: 4000
    t.string  "glucoseUnit",         limit: 4000
    t.decimal "height",                           precision: 6, scale: 2
    t.string  "heightUnit",          limit: 4000
    t.string  "locale",              limit: 4000
    t.date    "memberSince"
    t.integer "offsetFromUTCMillis", limit: 4
    t.string  "startDayOfWeek",      limit: 4000
    t.decimal "strideLengthRunning",              precision: 8, scale: 2
    t.decimal "strideLengthWalking",              precision: 8, scale: 2
    t.string  "timezone",            limit: 4000
    t.string  "waterUnit",           limit: 4000
    t.string  "waterUnitName",       limit: 4000
    t.decimal "weight",                           precision: 6, scale: 2
    t.string  "weightUnit",          limit: 4000
    t.string  "daily_goals",         limit: 4000,                         default: "{}", null: false
    t.string  "weekly_goals",        limit: 4000,                         default: "{}", null: false
    t.string  "fat_goal",            limit: 4000,                         default: "{}", null: false
    t.string  "weight_goal",         limit: 4000,                         default: "{}", null: false
    t.string  "food_goals",          limit: 4000,                         default: "{}", null: false
    t.string  "food_plan",           limit: 4000,                         default: "{}", null: false
  end

  add_index "fitbit_users", ["encodedId"], name: "index_fitbit_users_on_encodedId", unique: true

  create_table "fitbit_weight_logs", id: false, force: :cascade do |t|
    t.integer "user_id", limit: 4
    t.decimal "bmi",                  precision: 4, scale: 2
    t.date    "date"
    t.integer "logId",   limit: 4,                            null: false
    t.string  "time",    limit: 4000
    t.integer "weight",  limit: 4
    t.string  "source",  limit: 4000
  end

  add_index "fitbit_weight_logs", ["logId"], name: "index_fitbit_weight_logs_on_logId"
  add_index "fitbit_weight_logs", ["user_id"], name: "index_fitbit_weight_logs_on_user_id"

  create_table "underarmour_actigraphies", force: :cascade do |t|
    t.string   "user_id",            limit: 4000
    t.date     "date"
    t.text     "aggregates",         limit: 2147483647, default: "{}", null: false
    t.text     "metrics",            limit: 2147483647, default: "{}", null: false
    t.text     "workouts",           limit: 2147483647, default: "[]", null: false
    t.string   "timezones",          limit: 4000,       default: "[]", null: false
    t.datetime "start_datetime_utc"
    t.datetime "end_datetime_utc"
  end

  add_index "underarmour_actigraphies", ["user_id", "date"], name: "index_underarmour_actigraphies_on_user_id_and_date"
  add_index "underarmour_actigraphies", ["user_id"], name: "index_underarmour_actigraphies_on_user_id"

  create_table "underarmour_bodymasses", id: false, force: :cascade do |t|
    t.string   "user_id",           limit: 4000
    t.integer  "external_id",       limit: 4
    t.decimal  "fat_mass",                       precision: 6, scale: 3
    t.decimal  "lean_mass",                      precision: 6, scale: 3
    t.decimal  "mass",                           precision: 6, scale: 3
    t.decimal  "bmi",                            precision: 4, scale: 2
    t.decimal  "fat_percent",                    precision: 4, scale: 2
    t.string   "datetime_timezone", limit: 4000
    t.datetime "datetime_utc"
    t.datetime "created_datetime"
    t.datetime "updated_datetime"
  end

  add_index "underarmour_bodymasses", ["external_id"], name: "index_underarmour_bodymasses_on_external_id", unique: true
  add_index "underarmour_bodymasses", ["user_id"], name: "index_underarmour_bodymasses_on_user_id"

  create_table "underarmour_data_sources", force: :cascade do |t|
    t.string  "user_id",         limit: 4000
    t.string  "name",            limit: 4000
    t.boolean "active"
    t.string  "advertised_name", limit: 4000
    t.string  "serial_number",   limit: 4000
    t.text    "_embedded",       limit: 2147483647, default: "{}", null: false
  end

  add_index "underarmour_data_sources", ["user_id"], name: "index_underarmour_data_sources_on_user_id"

  create_table "underarmour_devices", force: :cascade do |t|
    t.string "name",         limit: 4000
    t.string "description",  limit: 4000
    t.string "manufacturer", limit: 4000
    t.string "model",        limit: 4000, null: false
  end

  add_index "underarmour_devices", ["model"], name: "index_underarmour_devices_on_model", unique: true

  create_table "underarmour_users", id: false, force: :cascade do |t|
    t.string   "id",                         limit: 4000,                                              null: false
    t.string   "email",                      limit: 4000
    t.string   "username",                   limit: 4000
    t.string   "first_name",                 limit: 4000
    t.string   "last_name",                  limit: 4000
    t.string   "last_initial",               limit: 4000
    t.string   "display_name",               limit: 4000
    t.string   "gender",                     limit: 4000
    t.string   "preferred_language",         limit: 4000
    t.date     "birthdate"
    t.string   "goal_statement",             limit: 4000
    t.string   "profile_statement",          limit: 4000
    t.string   "hobbies",                    limit: 4000
    t.string   "introduction",               limit: 4000
    t.string   "display_measurement_system", limit: 4000
    t.string   "time_zone",                  limit: 4000
    t.decimal  "height",                                        precision: 5, scale: 2
    t.decimal  "weight",                                        precision: 5, scale: 2
    t.datetime "date_joined"
    t.datetime "last_login"
    t.string   "communication",              limit: 4000,                               default: "{}", null: false
    t.string   "location",                   limit: 4000,                               default: "{}", null: false
    t.string   "sharing",                    limit: 4000,                               default: "{}", null: false
    t.text     "_links",                     limit: 2147483647,                         default: "{}", null: false
  end

  add_index "underarmour_users", ["email"], name: "index_underarmour_users_on_email", unique: true
  add_index "underarmour_users", ["id"], name: "index_underarmour_users_on_id", unique: true

  create_table "users", force: :cascade do |t|
    t.string   "email",                        limit: 4000, default: "", null: false
    t.string   "encrypted_password",           limit: 4000, default: "", null: false
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",                limit: 4,    default: 0,  null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.datetime "created_at",                                             null: false
    t.datetime "updated_at",                                             null: false
    t.string   "provider",                     limit: 4000
    t.string   "uid",                          limit: 4000
    t.string   "fitbit_token",                 limit: 4000
    t.string   "fitbit_refresh_token",         limit: 4000
    t.datetime "fitbit_token_expires_at"
    t.string   "underarmour_token",            limit: 4000
    t.string   "underarmour_refresh_token",    limit: 4000
    t.datetime "underarmour_token_expires_at"
  end

  add_index "users", ["email"], name: "index_users_on_email", unique: true

  add_foreign_key "fitbit_devices", "fitbit_users", column: "user_id"
  add_foreign_key "fitbit_food_summaries", "fitbit_users", column: "user_id"
  add_foreign_key "fitbit_heart_activities", "fitbit_users", column: "user_id"
  add_foreign_key "fitbit_sleep_summaries", "fitbit_users", column: "user_id"
  add_foreign_key "underarmour_actigraphies", "underarmour_users", column: "user_id"
  add_foreign_key "underarmour_bodymasses", "underarmour_users", column: "user_id"
  add_foreign_key "underarmour_data_sources", "underarmour_users", column: "user_id"
end
