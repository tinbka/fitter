class Adapters::OAuth2 < Adapters::Base
  attr_reader :app, :user, :token
  
  def initialize(app:, user:, **config)
    @app  = app
    @user = user
    @token = AccessToken.new @app, @user, config.extract!(:service_name)
    super config
  end

  def request_with_refresh(url, params={})
    $log << "Using token: #{@token.token}"
    if @token.expired?
      # sync
      $log << "Refreshing with: #{@token.refresh_params}"
      resp = request_without_refresh \
        get_token_url,
        method: :post,
        body: @token.refresh_params,
        headers: {"Authorization" => "Basic #{Base64.strict_encode64 [@app[:id], @app[:secret]]*':'}"}
      @token.parse resp
      
      request url, params
    else
      params[:headers] ||= {}
      params[:headers]["Authorization" ] = "Bearer #{@token.token}"
      request_without_refresh(url, params)
    end
  end
  
  alias_method_chain :request, :refresh
  
  private
  
  def get_token_url(*)
    raise NotImplementedError, "you must implement #{self.class}#get_token_url"
  end
  
  
  class AccessToken
    attr_reader :user, :credentials, :token, :expires_at, :refresh_token
  
    # @param credentials [Hash] a container with app's :id and :secret
    # @param user [ORM Object] a user record that provides "token", "expires_at", and "refresh_token" fields
    # @param service_name [String] prefix (over "_") of these field names
    # @param expiration_margin [Integer] (60) Time in seconds for request to be processed with assurance that it will not expire
    def initialize(credentials, user, service_name: nil, expiration_margin: 60)
      @user = user
      @credentials = credentials
      @service_name = service_name
      @expiration_margin = expiration_margin
      
      @token = @user[[@service_name, :token].compact*'_']
      @expires_at = @user[[@service_name, :token_expires_at].compact*'_']
      @refresh_token = @user[[@service_name, :refresh_token].compact*'_']
    end
    
    def expired?
      $log <= @expires_at
      @expires_at <= Time.now + @expiration_margin
    end
    
    def refresh_params
      {client_id: @credentials[:id],
        client_secret: @credentials[:secret],
        grant_type: 'refresh_token',
        refresh_token: @refresh_token}
      end
      
    def refresh!(params)
      $log <= params
      @token = @user[[@service_name, :token].compact*'_'] = params[:token]
      @expires_at = @user[[@service_name, :token_expires_at].compact*'_'] = params[:expires_at]
      @refresh_token = @user[[@service_name, :refresh_token].compact*'_'] = params[:refresh_token]
      @user.save
    end
      
    def parse(response)
      json = JSON.parse response.body
      $log <= json
      if json['access_token']
        refresh! token: json['access_token'],
          expires_at: json['expires_at'] ? Time.at(json['expires_at']) : (Time.now + json['expires_in']), 
          refresh_token: json['refresh_token']
      else
        if json['errors']
          raise json['errors'][0]['errorType']
        else
          raise json.inspect
        end
      end
    end
    
  end
  
end