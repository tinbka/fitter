class Adapters::Base
  
  def initialize(**config)
    @config = config
  end
  
  def request(url, params)
    req = Typhoeus::Request.new(
      url.to_s,
      params.slice(
        :method, :params, :body, :headers,
        :verbose, :accept_encoding, :followlocation,
        :cookiefile, :cookiejar
      )
    )

    req.on_complete do |resp|
      if resp.timed_out?
         raise TimeoutError, "request timed out"
      elsif resp.response_code == 0
        raise ConnectionFailed, resp.return_message
      end
    end
    
    req.run
  end
  
  
  class TimeoutError < Typhoeus::Errors::TyphoeusError
  end
  
  class ConnectionFailed < Typhoeus::Errors::TyphoeusError
  end
  
  class ClientError < Typhoeus::Errors::TyphoeusError
  end
  
end