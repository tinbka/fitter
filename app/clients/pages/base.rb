class Pages::Base
  attr_reader :response, :body, :data, :errors
  
  def initialize(response)
    @response = response
    @body = response.body
    @errors = []
    
    if @response.code == 200
      @data = parse
    else
      @errors << "unexpected response, code: #{response.code}"
      @data = parse raise_error: false
      @errors << (@data ? "content: #{@data['errors'] || @data['_diagnostics']}" : "unexpected response content type")
    end
  end
  
  def parse(raise_error: true)
    JSON.parse @body
  rescue JSON::ParserError, TypeError
    raise $!.class, $!.message if raise_error
  end
  
end