module Fitbit::Concerns::Food
  
  # Get a body goal object.
  # @see https://dev.fitbit.com/docs/food-logging/
  # The Get Food Goals endpoint returns a user's current daily calorie consumption goal and/or food Plan in the format requested.
  # 
  # Related tables: [fitbit_users]
  def food_goals
    api "/1/user/-/foods/log/goal.json"
  end
  
  # Get a food summary for a given date.
  # @see https://dev.fitbit.com/docs/food-logging/
  # The Get Weight Logs API retrieves a list of all user's body weight log entries for a given day using units in the unit systems which corresponds to the Accept-Language header provided. Body weight log entries are available only to authorized user. Body weight log entries in response are sorted exactly the same as they are presented on the Fitbit website.
  # 
  # Related tables: [fitbit_food_summaries]
  #
  # @option date [Date] The date of records to be returned.
  def daily_food_summary(date: Date.today)
    api "/1/user/-/foods/log/date/#{date.iso8601}.json"
  end
  
  # Get a water summary for a given date.
  # @see https://dev.fitbit.com/docs/food-logging/
  # The Get Water Logs endpoint retrieves a summary and list of a user's water log entries for a given day in the format requested using units in the unit system that corresponds to the Accept-Language header provided. Water log entries are available only to an authorized user. If you need to fetch only total amount of water consumed, you can use the Get Food endpoint. Water log entries in response are sorted exactly the same as they are presented on the Fitbit website.
  # 
  # Related tables: [fitbit_food_summaries]
  #
  # @option date [Date] The date of records to be returned.
  def daily_water_summary(date: Date.today)
    api "/1/user/-/foods/log/water/date/#{date.iso8601}.json"
  end
  
end