module Fitbit::Concerns::Body
  
  # Get a body goal object
  # @see https://dev.fitbit.com/docs/body/#get-body-goals
  # The Get Body Goals API retrieves a user's current body fat percentage or weight goal using units in the unit systems that corresponds to the Accept-Language header provided in the format requested.
  # 
  # Related tables: [fitbit_users]
  #
  # @option type [String] "weight" or "fat"
  def body_goal(type:)
    api "/1/user/-/body/log/#{type}/goal.json"
  end
  
  %w(weight fat).each do |type|
    class_eval %{
      # Get a #{type} body goal object
      # @reference https://dev.fitbit.com/docs/body/#get-body-goals
      def #{type}_goal
        body_goal(type: "#{type}")
      end
    }
  end
  
  # Get a list of weight log objects
  # @see https://dev.fitbit.com/docs/body/#get-weight-logs
  # The Get Weight Logs API retrieves a list of all user's body weight log entries for a given day using units in the unit systems which corresponds to the Accept-Language header provided. Body weight log entries are available only to authorized user. Body weight log entries in response are sorted exactly the same as they are presented on the Fitbit website.
  # 
  # Related tables: [fitbit_weight_logs]
  #
  # @option date [Date] The date of logs. If date is provided, other options are ignored.
  # @option period [String] The date range period. One of 1d, 7d, 30d, 1w, 1m. If period is provided, end_date are ignored.
  # @option base_date [Date] The end date when period is provided; range start date when a date range is provided.
  # @option end_date [Date] Range end date when date range is provided. Note: The period must not be longer than 31 days.
  def weight_logs(date: nil, period: '1m', base_date: Date.today, end_date: nil)
    if date
      api "/1/user/-/body/log/weight/date/#{date.iso8601}.json"
    elsif period
      api "/1/user/-/body/log/weight/date/#{base_date.iso8601}/#{period}.json"
    elsif end_date
      api "/1/user/-/body/log/weight/date/#{base_date.iso8601}/#{end_date.iso8601}.json"
    else
      raise ArgumentError, 'must specify date or (base_date and (period or end_date))'
    end
  end
  
end