module Fitbit::Concerns::Activities
  
  # Fitbit gives with one API request a summary only for a day.
  # @see https://dev.fitbit.com/docs/activity/#get-daily-activity-summary
  # The Get Daily Activity Summary endpoint retrieves a summary and list of a user's activities and activity log entries for a given day in the format requested using units in the unit system which corresponds to the Accept-Language header provided.
  # 
  # Related tables: [fitbit_activities]
  #
  # @option date [Date] The date of activity.
  def daily_activity_summary(date: Date.today)
    api "/1/user/-/activities/date/#{date.iso8601}.json"
  end
  
  # @deprecated due to a limit of requests for an application
  # Get all activity summary for the last 30 days. See #daily_activity_summary
  # @option end_date [Date] The date of activity until which 30 days will be iterated.
  def monthly_activity_summary(end_date: Date.today)
    (0...30).to_a.map {|i|
      daily_activity_summary(date: end_date - i.days).merge(date: end_date - i.days)
    }
  end
  
  # Get a specific activity for a period of time.
  # @see https://dev.fitbit.com/docs/activity/#activity-time-series
  # The Get Activity Time Series endpoint returns time series data in the specified range for a given resource in the format requested using units in the unit system that corresponds to the Accept-Language header provided.
  # 
  # @param resource_path [String] One of the list at @see
  # @option base_date [Date | "today"]	The range start date.
  # @option end_date	[Date] The end date of the range.
  # @option date [Date | "today"] The end date of the period.
  # @option period [String] The range for which data will be returned. Options are 1d, 7d, 30d, 1w, 1m, 3m, 6m, 1y, or max.
  #
  # @default get activity for all the time the user is registered.
  def activity(resource_path, date: 'today', period: 'max', base_date: nil, end_date: nil)
    if base_date and end_date
      if base_date == 'today'
        base_date = Date.today
      end
      api "/1/user/-/#{resource_path}/date/#{base_date.iso8601}/#{end_date.iso8601}.json"
    elsif date and period
      if date == 'today'
        date = Date.today
      end
      api "/1/user/-/#{resource_path}/date/#{date.iso8601}/#{period}.json"
    else
      raise ArgumentError, 'must specify (base_date and end_date) or (date and period)'
    end
  end
  
  %w(minutesLightlyActive minutesFairlyActive minutesVeryActive calories activityCalories).each do |activity_type|
    class_eval %{
      # Get the #{activity_type} activity for a period of time.
      # @see https://dev.fitbit.com/docs/activity/#activity-time-series
      # @see #activity for option list
      def #{activity_type.underscore}(date: 'today', period: 'max', base_date: nil, end_date: nil)
        activity("activities/#{activity_type}", date: date, period: period, base_date: base_date, end_date: end_date)
      end
    }
  end
  
  # Get a list of activity log objects
  # @see https://dev.fitbit.com/docs/activity/#get-activity-logs-list
  # The Get Activity Logs List endpoint retrieves a list of a user's activity log entries before or after a given day with offset and limit using units in the unit system which corresponds to the Accept-Language header provided.
  # 
  # Related tables: [fitbit_activities]
  #
  # @option before_date [DateTime | Date] Either beforeDate or afterDate should be specified.
  # @option after_date [DateTime | Date] Either beforeDate or afterDate should be specified.
  # @option sort [String] The sort order of entries by date asc (ascending) or desc (descending).
  # @option offset [Integer] The offset number of entries.
  # @option limit [Integer] The max of the number of entries returned (maximum: 100).
  def activity_logs(before_date: nil, after_date: nil, sort: :desc, offset: 0, limit: 100)
    params = {}
    params[:beforeDate] = before_date.in_time_zone('GMT').iso8601[/[^+]+/] if before_date
    params[:afterDate] = after_date.in_time_zone('GMT').iso8601[/[^+]+/] if after_date
    api "/1/user/-/activities/list.json", params: params.merge(sort: sort, offset: offset, limit: limit)
  end
  
  # Get an activity goals object
  # @see https://dev.fitbit.com/docs/activity/#activity-goals
  # The Get Activity Goals retrieves a user's current daily or weekly activity goals using measurement units as defined in the unit system, which corresponds to the Accept-Language header provided.
  # 
  # Related tables: [fitbit_users]
  #
  # @option period [String] "daily" or "weekly"
  def activity_goals(period:)
    api "/1/user/-/activities/goals/#{period}.json"
  end
  
  %w(daily weekly).each do |period|
    class_eval %{
      # Get a #{period} activity goals object
      # @see https://dev.fitbit.com/docs/activity/#activity-goals
      def #{period}_activity_goals
        activity_goals(period: "#{period}")
      end
    }
  end
  
  # Get a nested list of all activity categories
  # @see https://dev.fitbit.com/docs/activity/#activity-types
  # Get a tree of all valid Fitbit public activities from the activities catalog as well as private custom activities the user created in the format requested. If the activity has levels, also get a list of activity level details.
  # Typically, an applications retrieve the complete list of activities once at startup to cache and show in the UI later.
  # 
  # Related tables: [fitbit_activity_categories], [fitbit_activity_types]
  def activity_categories
    api "/1/activities.json"
  end
  
  # Get time series of heart rate for a period of time.
  # @see https://dev.fitbit.com/docs/heart-rate/
  # The Get Heart Rate Time Series endpoint returns time series data in the specified range for a given resource in the format requested using units in the unit systems that corresponds to the Accept-Language header provided.
  # If you specify earlier dates in the request, the response will retrieve only data since the user's join date or the first log entry date for the requested collection.
  # 
  # Related tables: [fitbit_heart_activities]
  # 
  # @option base_date [Date | "today"]	The range start date.
  # @option end_date	[Date] The end date of the range.
  # @option date [Date | "today"] The end date of the period.
  # @option period [String] The range for which data will be returned. Options are 1d, 7d, 30d, 1w, 1m.
  #
  # @default get time series for the last month.
  def heart_activity(date: 'today', period: '1m', base_date: nil, end_date: nil)
    activity 'activities/heart', date: date, period: period, base_date: base_date, end_date: end_date
  end
  alias heart_rate_time_series heart_activity
  
end