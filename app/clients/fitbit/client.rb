class Fitbit::Client < Adapters::OAuth2
  include Fitbit::Concerns::Activities
  include Fitbit::Concerns::Body
  include Fitbit::Concerns::Food
  
  attr_reader :last_response, :last_response_parsed
  
  # @param user [User] A user on behalf of which API will be requested.
  # @option lang [String] (optional) The measurement unit system to use for response values. See https://dev.fitbit.com/docs/basics/ (Units section)
  def initialize(user:, **config)
    @lang = config[:lang]
    super app: Settings.apps.fitbit, user: user, service_name: :fitbit, **config
  end
  
  def host
    'https://api.fitbit.com'
  end
  
  def get_token_url
    host + '/oauth2/token'
  end
  
  
  # Base request method
  def api(path, params={})
    if @lang
      params[:headers] ||= {}
      params[:headers]['Accept-Language'] = @lang
    end
    
    @last_response = res = request host + path, params
    page = Pages::Base.new(res)
    @last_response_parsed = page.data
    
    if page.errors.any?
      raise ClientError, page.errors.to_sentence
    else
      page.data
    end
  end
  
  # All the user profile data including user's badges
  # @see https://dev.fitbit.com/docs/user/#get-profile
  # The Get Profile endpoint returns a user's profile. The authenticated owner receives all values. However, the authenticated user's access to other users' data is subject to those users' privacy settings. Numerical values are returned in the unit system specified in the Accept-Language header.
  #
  # Related tables: [fitbit_user], [fitbit_badges], [fitbit_badges_fitbit_user]
  def profile
    api "/1/user/-/profile.json"
  end
  
  # Get sleep summary for a given date.
  # @see https://dev.fitbit.com/docs/sleep/
  # The Get Sleep Logs endpoint returns a summary and list of a user's sleep log entries as well as minute by minute sleep entry data for a given day in the format requested. The endpoint response includes summary for all sleep log entries for the given day (including naps.) If you need to fetch data only for the user's main sleep event, you can send the request with isMainSleep=true or use a Time Series call.
  # The relationship between sleep log entry properties is expressed with the following equation:
  #   timeInBed = minutesToFallAsleep + minutesAsleep + minutesAwake + minutesAfterWakeup
  # 
  # Related tables: [fitbit_sleep_logs]
  #
  # @option date [Date] The date of sleep records.
  # @option main_sleep [Boolean] Whether you need to fetch data only for the user's main sleep event.
  def daily_sleep_summary(date: Date.today, main_sleep: false)
    api "/1/user/-/sleep/date/#{date.iso8601}.json", params: {isMainSleep: main_sleep.to_s}
  end
  
  # Get user's devices list with their current conditions.
  # @see https://dev.fitbit.com/docs/devices/
  # The Get Device endpoint returns a list of the Fitbit devices connected to a user's account.
  #
  # Related tables: [fitbit_devices]
  def devices
    api "/1/user/-/devices.json"
  end
  
end