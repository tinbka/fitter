class Underarmour::Client < Adapters::OAuth2
  attr_reader :last_response, :last_response_parsed
  
  # @param user [User] A user on behalf of which API will be requested.
  # @option lang [String] (optional) The measurement unit system to use for response values. See https://dev.fitbit.com/docs/basics/ (Units section)
  def initialize(user:, **config)
    @lang = config[:lang]
    super app: Settings.apps.underarmour, user: user, service_name: :underarmour, **config
  end
  
  def host
    'https://api.ua.com'
  end
  
  def get_token_url
    host + '/v7.1/oauth2/access_token/'
  end
  
  
  # Base request method
  def api(path, params={})
    params[:headers] ||= {}
    params[:headers]['api-key'] = @app.id
    if @lang
      params[:headers]['Accept-Language'] = @lang
    end
    if params[:params]
      params[:params].each_with_object(params[:params]) {|(k,v),h|
        case v
        when Date then h[k] = v.iso8601
        when Time then h[k] = v.utc.iso8601
        when Array then h[k] = v*','
        end
      }
    end
    
    @last_response = res = request host + path, params
    page = Pages::Base.new(res)
    @last_response_parsed = page.data
    
    if page.errors.any?
      raise ClientError, page.errors.to_sentence
    else
      page.data
    end
  end
  
  # Get User information.
  # @see https://developer.underarmour.com/docs/v71_User
  #
  # Related tables: [underarmour_user]
  def profile
    api "/v7.1/user/self/"
  end
  
  # Get user stats summary
  # @see https://developer.underarmour.com/docs/v71_User_Stats
  # Returns aggregated statistics across activities the user has completed. By default, statistics are broken down by the type of activity and grouped into various date ranges depending on the query parameters.
  #
  # @option aggregate_by_period [String] Valid values: day, week, month, year, lifetime. Defaults to lifetime. Defines the spans over which aggregates are generated. Values month and year are calendar-based. week is based on a Sunday start.
  # @option end_date [Date] The end-bounding date to consider when generating aggregates. Workouts on this date are NOT counted, but any workouts earlier than this date (back to and including the start_date) are. Defaults to today + 1 day.
  # @option start_date [Date] The start-bounding date to consider when generating aggregates. Workouts on or after this date are counted (up to but not including the end_date). Defaults to end_date minus a variable number of days depending on the value of aggregate_by_period.
  # @option include_summary_stats [Boolean] The values normally returned in the stats array are broken down by activity_type. If this parameter is included and set to true, an additional summary_stats property will be included which rolls together all activity types for each aggregation period.
  #
  # No tables implemented: no data entries.
  def stats(**options)
    api "/v7.1/user_stats/#{@user.uid}/", params: options.slice(:aggregate_by_period, :end_date, :start_date, :include_summary_stats)
  end
  
  # Get user's actigraphy records
  # @see https://developer.underarmour.com/docs/v71_User_Stats
  # Returns aggregated statistics across activities the user has completed. By default, statistics are broken down by the type of activity and grouped into various date ranges depending on the query parameters.
  #
  # @option start_date [Date] Return Actigraphy data after this date, inclusively.
  # @option end_date [Date] Return Actigraphy data before this date, inclusively. Defaults to current date.
  # @option time_series_interval [Integer] Specify the interval in seconds between points in time series. Allowed values are 900, 1800, 3600. Default is 900.
  #
  # Related tables: [underarmour_actigraphies]
  def actigraphy(start_date: 1.month.ago.to_date, **options)
    api "/v7.1/actigraphy/", params: {user: @user.uid, start_date: start_date}.merge(options.slice(:end_date, :time_series_interval))
  end
  
  # Get user's body mass records
  # @see https://developer.underarmour.com/docs/v71_BodyMass
  # This resource allows creation and modification of Body Mass entries. Body Mass relates to a user's weight, mass, fat percentage, and body mass index.
  #
  # @option target_start_datetime [DateTime] Indicates where to start the search.
  # @option target_end_datetime [DateTime] Indicates where to end the search. Defaults to current time.
  #
  # Related tables: [underarmour_bodymasses]
  def bodymass(target_start_datetime: 1.month.ago, **options)
    api "/v7.1/bodymass/", params: {target_start_datetime: target_start_datetime}.merge(options.slice(:target_end_datetime))
  end
  
  # Get user's sleep records
  # @see https://developer.underarmour.com/docs/v71_Sleep
  # This resource allows creation, modification, and deletion of Sleep data associated with a User. Sleep data consists of sleep duration, details, aggregates, and time series data.
  #
  # @option target_start_datetime [DateTime] A datetime indicating where to start the search. Sleeps are searched by end time with target_start_datetime inclusive.
  # @option target_end_datetime [DateTime] A datetime indicating where to end the search. Defaults to current time. Sleeps are searched by end time with target_end_datetime exclusive.
  # @option field_set [Array] Options available are: ['time_series'].
  #
  # No tables implemented: no data entries.
  def sleep(target_start_datetime: 1.month.ago, target_end_datetime: Date.tomorrow, field_set: ['time_series'])
    api "/v7.1/sleep/", params: {target_start_datetime: target_start_datetime.to_time, target_end_datetime: target_end_datetime.to_time, field_set: field_set}
  end
  
  # @see https://developer.underarmour.com/docs/v71_Heart_Rate_Zone
  # This resource provides access to a user's Heart Rate Zone settings.
  #
  # No tables implemented: no data entries.
  def heart_rate_zones
    api "/v7.1/heart_rate_zones/", params: {user: @user.uid}
  end
  
  # Retrieve a list of Workout entities using valid query parameters.
  # @see https://developer.underarmour.com/docs/v71_Workout
  # This resource allows creation, retrieval, modification and deletion of a user's Workouts. A Workout is considered deliberate physical activity, as opposed to accumulating steps by passively walking over the course of the day.
  #
  # @option activity_type [Array<Integer>] Filter Workouts by the ActivityType ids specified.
  # @option updated_before [DateTime]
  # @option updated_after  [DateTime]
  # @option created_before [DateTime]
  # @option created_after  [DateTime]
  # @option started_before [DateTime]
  # @option started_after  [DateTime]
  #
  # No tables implemented: no data entries.
  # Not specified in the offer, required for association with #workout_heart_rate .
  def workouts(**options)
    api "/v7.1/workout/", params: {user: @user.uid}.merge(options.slice *%i(updated_before updated_after created_before created_after started_before started_after))
  end
  
  # Retrieve a Workout Heart Rate by workout id.
  # @see https://developer.underarmour.com/docs/v71_Workout_Heart_Rate
  # Provides Heart Rate Zone data associated with a workout.
  #
  # @param id [Integer] Associated workout id.
  #
  # No tables implemented: no data entries.
  def workout_heart_rate(id)
    api "/v7.1/workout_heart_rate/#{id}/"
  end
  
  # Get a list of Devices.
  # @see https://developer.underarmour.com/docs/v71_Device
  # The Device resource represents the hardware used to record data.
  #
  # @option manufacturer [String] Manufacturer name to filter by. At the moment only Samsung and Underarmour are known manufacturers.
  #
  # Related tables: [underarmour_devices]
  def devices(manufacturer:)
    api "/v7.1/device/", params: {manufacturer: manufacturer, limit: 40}
  end
  
  # Get a list of Data Sources owned by requesting user.
  # @see https://developer.underarmour.com/docs/v71_DataSource
  # The Data Source resource represents the unique combination of Device and Remote Connection for a user's data recording hardware.
  #
  # @option device [String] Valid id or href of the Device resource.
  #
  # Related tables: [underarmour_data_sources]
  def data_sources(**options)
    api "/v7.1/data_source/", params: options.slice(:device)
  end
  
end