class Fitbit::User < Fitbit::Base
  serialize :features, JSON
  serialize :daily_goals, JSON
  serialize :weekly_goals, JSON
  serialize :weight_goal, JSON
  serialize :fat_goal, JSON
  serialize :food_goals, JSON
  serialize :food_plan, JSON
  
  validates_uniqueness_of :encodedId, allow_blank: false
  
  belongs_to :user, class_name: '::User', primary_key: 'uid', foreign_key: 'encodedId'
  has_and_belongs_to_many :badges, join_table: 'fitbit_badges_fitbit_users'
  has_many :activity_summaries
  has_many :activities
  has_many :weight_logs
  has_many :food_summaries
  has_many :heart_activities
  has_many :sleep_summaries
  has_many :devices
  
  def to_s; "#{displayName} (##{encodedId})" end
  
  def client
    @client ||= Fitbit::Client.new(user: user)
  end
  
  # The main entry point from User
  def update_everything
    update_profile
    update_daily_activity_summary
    update_monthly_activity_log
    update_monthly_heart_activity
    update_monthly_weight_log
    update_daily_food_summary
    update_daily_sleep_summary
    update_devices
  end
  
  # Detailed methods
  
  def update_profile
    json = client.profile
    user_json = json['user']
    goals = %w(daily weekly).map {|period|
        ["#{period}_goals", client.activity_goals(period: period)['goals']]
      } +
      %w(fat weight).map {|type|
        ["#{type}_goal", client.body_goal(type: type)['goal']]
      }
    
    user_json.merge! goals.to_h
    food_goals = client.food_goals
    user_json['food_goals'] = food_goals['goals'] if food_goals['goals']
    user_json['food_plan'] = food_goals['foodPlan'] if food_goals['foodPlan']
    
    transaction do
      user_json['badges'] = user_json.delete('topBadges').map {|params| update_badge(params)}
      # don't try to write to the fields inexisted at API spec
      update user_json.slice('badges', *self.class.column_names)
    end
  end
  
  def update_badge(params)
    Fitbit::Badge.where(params.extract! 'encodedId').first_or_create.tap do |badge|
      badge.update params
    end.tap {|x| $log.debug x}
  rescue ActiveRecord::UnknownAttributeError
    logger.warn "#{$!.class}: #{$!.message}"
  end
  
  def update_daily_activity_summary(date: Date.today)
    json = client.daily_activity_summary(date: date)
    json.delete 'activities'
    activity_summaries.tap do |rel|
      if ass = rel.where(date: date).first
        ass.update json
      else
        rel.create json.merge(date: date)
      end
    end
  end
  
  def update_monthly_activity_log(date: Date.today)
    json = client.activity_logs(before_date: date)
    activities.tap do |rel|
      json['activities'].each {|params|
        if ass = rel.where(startDate: params['startDate']).first
          ass.update params
        else
          rel.create params
        end
      }
    end
  end
  
  def update_monthly_heart_activity(date: Date.today)
    json = client.heart_activity(date: date)
    heart_activities.tap do |rel|
      json['activities-heart'].each {|params|
        # it does not have a time value!
        params['date'] = params.delete 'dateTime'
        if ass = rel.where(date: params['date']).first
          ass.update params
        else
          rel.create params
        end
      }
    end
  end
  
  def update_monthly_weight_log(date: Date.today)
    json = client.weight_logs(base_date: date)
    weight_logs.tap do |rel|
      json['weight'].each {|params|
        if rel.where(logId: params['logId']).empty?
          rel.create params
        end
      }
    end
  end
  
  # Includes daily water summary
  def update_daily_food_summary(date: Date.today)
    json = client.daily_food_summary(date: date)
    water_json = client.daily_water_summary(date: date)
    json['water'] = water_json['water']
    food_summaries.tap do |rel|
      if fss = rel.where(date: date).first
        fss.update json
      else
        rel.create json.merge(date: date)
      end
    end
  end
  
  def update_daily_sleep_summary(date: Date.today)
    json = client.daily_sleep_summary(date: date)
    sleep_summaries.tap do |rel|
      if fss = rel.where(date: date).first
        fss.update json
      else
        rel.create json.merge(date: date)
      end
    end
  end
  
  def update_devices
    json = client.devices
    devices.tap do |rel|
      json.each {|params|
        if ass = rel.where(id: params['id']).first
          ass.update params
        else
          rel.create params
        end
      }
    end
  end
  
  # One-off method
  # The same types may apeear in different categories. We dup them with an according category id.
  # Whole JSON remains in the /db/fitbit_activity_categories.json for overview.
  def populate_activity_categories(categories = client.activity_categories['categories'], parent_id: nil, save_json: true)
    if save_json
      File.write "#{Rails.root.join}/db/fitbit_activity_categories.json", JSON.pretty_unparse(categories)
    end
      
    transaction do
      categories.each do |c|
        activities = c.delete 'activities'
        children = c.delete 'subCategories'
        cat = Fitbit::ActivityCategory.create c.merge parent_id: parent_id
        logger.debug cat
        activity_types = Fitbit::ActivityType.create(activities)
        logger.debug activity_types
        cat.activity_types = activity_types
        if children
          populate_activity_categories(children, parent_id: cat.id, save_json: false)
        end
      end
    end
  end
  
end