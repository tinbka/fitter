class Fitbit::ActivityCategory < Fitbit::Base
  self.primary_key = 'id'
  
  has_ancestry
  has_many :activity_types, class_name: 'Fitbit::ActivityType'
  
  def self.populate(user)
    user.populate_activity_categories
  end
end