class Fitbit::Device < Fitbit::Base
  self.inheritance_column = nil
  self.primary_key = 'id'
  serialize :features, JSON
  
  belongs_to :user, class_name: 'Fitbit::User'
end