class Fitbit::ActivitySummary < Fitbit::Base
  serialize :goals, JSON
  serialize :summary, JSON
  
  belongs_to :user, class_name: 'Fitbit::User'
end