class Fitbit::Base < ActiveRecord::Base
  self.abstract_class = true
  
  def self.table_name_prefix
    'fitbit_'
  end
  
  def setup_client(user: nil)
    Thread.current[:fitbit_client] = Fitbit::Client.new(user: user)
  end
  
  def client
    Thread.current[:fitbit_client]
  end
  
end