class Fitbit::ActivityType < Fitbit::Base
  self.primary_key = 'id'
  serialize :activityLevels, JSON
  
  belongs_to :activity_category, class_name: 'Fitbit::ActivityCategory'
end