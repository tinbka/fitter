class Fitbit::WeightLog < Fitbit::Base
  self.primary_key = 'logId'
  
  belongs_to :user, class_name: 'Fitbit::User'
end