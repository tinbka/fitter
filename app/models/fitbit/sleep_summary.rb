class Fitbit::SleepSummary < Fitbit::Base
  serialize :sleep, JSON
  serialize :summary, JSON
  
  belongs_to :user, class_name: 'Fitbit::User'
end