class Fitbit::HeartActivity < Fitbit::Base
  serialize :value, JSON
  
  belongs_to :user, class_name: 'Fitbit::User'
end