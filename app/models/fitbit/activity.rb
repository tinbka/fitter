class Fitbit::Activity < Fitbit::Base
  belongs_to :user, class_name: 'Fitbit::User'
end