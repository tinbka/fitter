class Fitbit::FoodSummary < Fitbit::Base
  serialize :foods, JSON
  serialize :water, JSON
  serialize :goals, JSON
  serialize :summary, JSON
  
  belongs_to :user, class_name: 'Fitbit::User'
end