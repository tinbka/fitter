class Fitbit::Badge < ActiveRecord::Base
  self.table_name = 'fitbit_badges'
  serialize :cheers, JSON
  # validates_uniqueness_of :encodedId, allow_blank: false
  
  has_and_belongs_to_many :user, class_name: 'Fitbit::User', join_table: 'fitbit_badges_fitbit_users'
end