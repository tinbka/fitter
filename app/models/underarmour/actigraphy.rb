class Underarmour::Actigraphy < Underarmour::Base
  serialize :aggregates, JSON
  serialize :metrics, JSON
  serialize :workouts, JSON
  serialize :timezones, JSON
  
  belongs_to :user
end