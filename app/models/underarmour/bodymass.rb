class Underarmour::Bodymass < Underarmour::Base
  self.primary_key = 'external_id'
  belongs_to :user
end