class Underarmour::Base < ActiveRecord::Base
  self.abstract_class = true
  
  def self.table_name_prefix
    'underarmour_'
  end
  
  def setup_client(user: nil)
    Thread.current[:underarmour_client] = Fitbit::Client.new(user: user)
  end
  
  def client
    Thread.current[:underarmour_client]
  end
  
end