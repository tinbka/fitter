class Underarmour::DataSource < Underarmour::Base
  self.primary_key = 'id'
  serialize :_embedded, JSON # {devices: <devices list>}
  belongs_to :user
end