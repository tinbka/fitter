class Underarmour::User < Underarmour::Base
  # It is not an auto-increment PK, it is called `id' at UA's User endpoint
  # And it is a string
  self.primary_key = 'id'
  
  serialize :communication, JSON
  serialize :location, JSON
  serialize :sharing, JSON
  serialize :_links, JSON
  
  belongs_to :user, class_name: '::User', primary_key: 'uid', foreign_key: 'id'
  has_many :actigraphies
  has_many :bodymasses
  has_many :data_sources
  
  def to_s; "#{display_name} (##{id})" end
  
  def client
    @client ||= Underarmour::Client.new(user: user)
  end
  
  # The main entry point from User
  def update_everything
    update_profile
    update_monthly_actigraphy
    update_monthly_bodymass
    update_data_sources
  end
  
  # Detailed methods
  
  def update_profile
    json = client.profile
    update json.slice *self.class.column_names
  end
  
  def update_monthly_actigraphy
    json = client.actigraphy
    actigraphies.tap do |rel|
      json['_embedded']['actigraphies'].each {|params|
        params.slice! *Underarmour::Actigraphy.column_names
        if ass = rel.where(date: params['date']).first
          ass.update params
        else
          rel.create params
        end
      }
    end
  end
  
  def update_monthly_bodymass
    json = client.bodymass
    bodymasses.tap do |rel|
      json['_embedded']['bodymasses'].each {|params|
        params.slice! *Underarmour::Bodymass.column_names
        if ass = rel.where(external_id: params['external_id']).first
          ass.update params
        else
          rel.create params
        end
      }
    end
  end
  
  def update_data_sources
    json = client.data_sources
    self.data_sources = json['_embedded']['data_sources'].map {|params|
      params.slice! *Underarmour::DataSource.column_names
      Underarmour::DataSource.create params
    }
  end
  
  # One-off method.
  # Fills the underarmour_devices table with the device models known to UA.
  def populate_devices(manufacturers: %w(samsung underarmour client))
    lists_json = manufacturers.map {|x| client.devices manufacturer: x}
    Underarmour::Device.tap do |rel|
      lists_json.each {|json|
        json['_embedded']['devices'].each {|params|
          params.slice! *Underarmour::Device.column_names
          if ass = rel.where(model: params['model']).first
            ass.update params
          else
            rel.create params
          end
        }
      }
    end
  end
  
end