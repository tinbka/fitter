class Users::OmniauthCallbacksController < Devise::OmniauthCallbacksController
  
  def fitbit
    @user = User.from_omniauth(request.env["omniauth.auth"])

    if @user.persisted?
      sign_in_and_redirect @user, :event => :authentication # this will throw if @user is not activated
      set_flash_message(:notice, :success, :kind => "Fitbit") if is_navigational_format?
    else
      session["devise.fitbit_data"] = request.env["omniauth.auth"]
      redirect_to new_user_registration_url
    end
  end
  
  def underarmour
    @user = User.from_omniauth(request.env["omniauth.auth"])

    if @user.persisted?
      sign_in_and_redirect @user, :event => :authentication # this will throw if @user is not activated
      set_flash_message(:notice, :success, :kind => "UnderArmour") if is_navigational_format?
      @user.update_from_provider
    else
      session["devise.underarmour_data"] = request.env["omniauth.auth"]
      redirect_to new_user_registration_url
    end
  end

  def failure
    redirect_to root_path
  end
  
end