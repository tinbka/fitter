ActiveAdmin.register Fitbit::User do

  index do
    selectable_column
    
    column(:encodedId) {|i| auto_link i, i.encodedId}
    column :displayName
    column :fullName
    column :age
    column :gender
    column :memberSince
    column :timezone
    column :height
    column :heightUnit
    column :weight
    column :weightUnit
    
    actions
  end

  filter :displayName
  filter :fullName
  filter :age
  filter :gender
  
  show title: :displayName do |user|
    panel "Fitbit user #{user} details" do
      attributes_table_for user do
        json_cols, simple_cols = Fitbit::User.columns.partition {|x| x.cast_type.respond_to? :coder and x.cast_type.coder == ActiveRecord::Coders::JSON}
        simple_cols.map(&:name).each {|name| row name}
        json_cols.map(&:name).each {|name| row(name) {|i| JSON.pretty_unparse i[name]}}
      end
    end
    
    panel "Activity summary records by date" do
      table_for user.activity_summaries do |t|
        t.column(:date) {|i| auto_link i, i.date}
        t.column(:goals) {|i| JSON.pretty_unparse i.goals}
        t.column(:summary) {|i| JSON.pretty_unparse i.summary}
      end
    end
    
    panel "Activity log by date" do
      table_for user.activities do |t|
        column(:startDate) {|i| auto_link i, i.startDate}
        column(:startTime)
        column(:name)
        column(:description)
      end
    end
    
    panel "Heart activity / Heart rate time series by date" do
      table_for user.heart_activities do |t|
        t.column(:date) {|i| auto_link i, i.date}
        t.column(:value) {|i| JSON.pretty_unparse i.value}
      end
    end
    
    panel "Badges" do
      table_for user.badges do |t|
        column(:name) {|i| auto_link i, i.name}
        column(:category)
        column(:description)
      end
    end
    
    panel "Weight log" do
      table_for user.weight_logs do |t|
        column(:logId) {|i| auto_link i, i.logId}
        column :bmi
        column :date
        column :time
        column :weight
        column :source
      end
    end
    
    panel "Food summary records by date" do
      table_for user.food_summaries do |t|
        t.column(:date) {|i| auto_link i, i.date}
        t.column(:foods) {|i| JSON.pretty_unparse i.foods}
        t.column(:water) {|i| JSON.pretty_unparse i.water}
        t.column(:goals) {|i| JSON.pretty_unparse i.goals}
        t.column(:summary) {|i| JSON.pretty_unparse i.summary}
      end
    end
    
    panel "Sleep summary records by date" do
      table_for user.sleep_summaries do |t|
        t.column(:date) {|i| auto_link i, i.date}
        t.column(:sleep) {|i| JSON.pretty_unparse i.sleep}
        t.column(:summary) {|i| JSON.pretty_unparse i.summary}
      end
    end
    
    panel "Devices" do
      table_for user.devices do |t|
        column(:id) {|i| auto_link i, i.id}
        column :battery
        column :deviceVersion
        column :lastSyncTime
        column :mac
        column :type
        column(:features) {|i| JSON.pretty_unparse i.features}
      end
    end
    
  end

end
