ActiveAdmin.register Fitbit::ActivitySummary do

  index do
    selectable_column
    
    column(:date) {|i| auto_link i, i.date}
    column(:user) {|i| auto_link i.user}
    column(:goals) {|i| JSON.pretty_unparse i.goals}
    column(:summary) {|i| JSON.pretty_unparse i.summary}
    
    actions
  end

  filter :date
  filter :user
  
  show title: ->(as) {"#{as.user.displayName} activity summary for #{as.date}"} do |as|
    panel "Details" do
      attributes_table_for as do
        row(:date)
        row(:user) {|i| auto_link i.user}
        row(:goals) {|i| JSON.pretty_unparse i.goals}
        row(:summary) {|i| JSON.pretty_unparse i.summary}
      end
    end
  end

end
