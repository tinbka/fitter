ActiveAdmin.register Fitbit::Badge do

  index do
    selectable_column
    
    column(:name) {|i| auto_link i, i.name}
    column(:category)
    column(:description)
    
    actions
  end

  filter :startDate
  filter :name
  filter :user
  
  show do |b|
    panel "Details" do
      attributes_table_for b do
        row :badgeGradientEndColor
        row :badgeGradientStartColor
        row :badgeType
        row :category
        row(:cheers) {|i| JSON.pretty_unparse i.cheers}
        row :dateTime
        row :description
        row :earnedMessage
        row :encodedId
        row :image100px
        row :image125px
        row :image300px
        row :image50px
        row :image75px
        row :marketingDescription
        row :mobileDescription
        row :name
        row :shareImage640px
        row :shareText
        row :shortDescription
        row :shortName
        row :timesAchieved
        row :unit
        row :value
      end
    end
  end

end
