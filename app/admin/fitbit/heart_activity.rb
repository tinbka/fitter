ActiveAdmin.register Fitbit::HeartActivity do

  index do
    selectable_column
    
    column(:date) {|i| auto_link i, i.date}
    column(:user) {|i| auto_link i.user}
    column(:value) {|i| JSON.pretty_unparse i.value}
    
    actions
  end

  filter :date
  filter :user
  
  show title: ->(x) {"#{x.user.displayName} heart activity for #{x.date}"} do |x|
    panel "Details" do
      attributes_table_for x do
        row(:date)
        row(:user) {|i| auto_link i.user}
        row(:value) {|i| JSON.pretty_unparse i.value}
      end
    end
  end

end
