ActiveAdmin.register Fitbit::WeightLog do

  index do
    selectable_column
    
    column(:logId) {|i| auto_link i, i.logId}
    column(:user) {|i| auto_link i.user}
    column :bmi
    column :date
    column :time
    column :weight
    column :source
    
    actions
  end

  filter :date
  filter :source
  filter :user
  
  show title: ->(x) {"#{x.user.displayName} weight log ##{x.logId}"} do |x|
    panel "Details" do
      attributes_table_for x do
        row :logId
        row(:user) {|i| auto_link i.user}
        row :bmi
        row :date
        row :time
        row :weight
        row :source
      end
    end
  end

end
