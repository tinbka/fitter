ActiveAdmin.register Fitbit::Activity do

  index do
    selectable_column
    
    column(:name) {|i| auto_link i, i.name}
    column(:user) {|i| auto_link i.user}
    column(:startTime)
    column(:startDate)
    column(:description)
    
    actions
  end

  filter :startDate
  filter :name
  filter :user
  
  show title: ->(a) {"#{a.user.displayName} activity for #{a.startDate}"} do |a|
    panel "Details" do
      attributes_table_for a do
        row :activityId
        row :activityParentId
        row(:user) {|i| auto_link i.user}
        row :activityParentName
        row :calories
        row :description
        row :distance
        row :duration
        row :hasStartTime
        row :isFavorite
        row :lastModified
        row :logId
        row :name
        row :startDate
        row :startTime
        row :steps
      end
    end
  end

end
