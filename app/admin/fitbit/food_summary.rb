ActiveAdmin.register Fitbit::FoodSummary do

  index do
    selectable_column
    
    column(:date) {|i| auto_link i, i.date}
    column(:user) {|i| auto_link i.user}
    column(:foods) {|i| JSON.pretty_unparse i.foods}
    column(:water) {|i| JSON.pretty_unparse i.water}
    column(:goals) {|i| JSON.pretty_unparse i.goals}
    column(:summary) {|i| JSON.pretty_unparse i.summary}
    
    actions
  end

  filter :date
  filter :user
  
  show title: ->(x) {"#{x.user.displayName} food summary for #{x.date}"} do |x|
    panel "Details" do
      attributes_table_for x do
        row(:date)
        row(:user) {|i| auto_link i.user}
        row(:foods) {|i| JSON.pretty_unparse i.foods}
        row(:water) {|i| JSON.pretty_unparse i.water}
        row(:goals) {|i| JSON.pretty_unparse i.goals}
        row(:summary) {|i| JSON.pretty_unparse i.summary}
      end
    end
  end

end
