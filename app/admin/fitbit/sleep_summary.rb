ActiveAdmin.register Fitbit::SleepSummary do

  index do
    selectable_column
    
    column(:date) {|i| auto_link i, i.date}
    column(:user) {|i| auto_link i.user}
    column(:sleep) {|i| JSON.pretty_unparse i.sleep}
    column(:summary) {|i| JSON.pretty_unparse i.summary}
    
    actions
  end

  filter :date
  filter :user
  
  show title: ->(x) {"#{x.user.displayName} activity summary for #{x.date}"} do |x|
    panel "Details" do
      attributes_table_for x do
        row(:date)
        row(:user) {|i| auto_link i.user}
        row(:sleep) {|i| JSON.pretty_unparse i.sleep}
        row(:summary) {|i| JSON.pretty_unparse i.summary}
      end
    end
  end

end
