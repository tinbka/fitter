ActiveAdmin.register Underarmour::DataSource do

  index do
    selectable_column
    
    column(:id) {|i| auto_link i, i.id}
    column(:user) {|i| auto_link i.user}
    column(:name)
    column(:active)
    column(:advertised_name)
    column(:serial_number)
    column(:_embedded) {|i| JSON.pretty_unparse i._embedded}
    
    actions
  end

  filter :user
  
  show title: ->(x) {"#{x.user.display_name} data sources"} do |x|
    panel "Details" do
      attributes_table_for x do
        row(:id)
        row(:user) {|i| auto_link i.user}
        row(:name)
        row(:active)
        row(:advertised_name)
        row(:serial_number)
        row(:_embedded) {|i| JSON.pretty_unparse i._embedded}
      end
    end
  end

end
