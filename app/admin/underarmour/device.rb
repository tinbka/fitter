ActiveAdmin.register Underarmour::Device do

  index do
    selectable_column
    
    column(:model) {|i| auto_link i, i.model}
    column(:name)
    column(:description)
    column(:manufacturer)
    
    actions
  end

  filter :manufacturer
  
  show do |x|
    panel "Details" do
      attributes_table_for x do
        row(:model)
        row(:name)
        row(:description)
        row(:manufacturer)
      end
    end
  end

end
