ActiveAdmin.register Underarmour::Actigraphy do

  index do
    selectable_column
    
    column(:date) {|i| auto_link i, i.date}
    column(:user) {|i| auto_link i.user}
    column(:aggregates) {|i| JSON.pretty_unparse i.aggregates}
    column(:metrics) {|i| JSON.pretty_unparse i.metrics}
    column(:workouts) {|i| JSON.pretty_unparse i.workouts}
    
    actions
  end

  filter :date
  filter :user
  
  show title: ->(x) {"#{x.user.display_name} actigraphy records for #{x.date}"} do |x|
    panel "Details" do
      attributes_table_for x do
        row(:date)
        row(:user) {|i| auto_link i.user}
        row(:aggregates) {|i| JSON.pretty_unparse i.aggregates}
        row(:metrics) {|i| JSON.pretty_unparse i.metrics}
        row(:workouts) {|i| JSON.pretty_unparse i.workouts}
        row(:timezones) {|i| JSON.pretty_unparse i.timezones}
        row(:start_datetime_utc)
        row(:end_datetime_utc)
      end
    end
  end

end
