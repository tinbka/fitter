ActiveAdmin.register Underarmour::User do

  index do
    selectable_column
    id_column
    
    column :email
    column :username
    column :display_name
    column :birthdate
    column :gender
    column :date_joined
    column :time_zone
    column :height
    column :weight
    
    actions
  end

  filter :display_name
  filter :username
  filter :email
  filter :birthdate
  filter :gender
  
  show title: :display_name do |user|
    panel "Underarmour user #{user} details" do
      attributes_table_for user do
        json_cols, simple_cols = Underarmour::User.columns.partition {|x| x.cast_type.respond_to? :coder and x.cast_type.coder == ActiveRecord::Coders::JSON}
        simple_cols.map(&:name).each {|name| row name}
        json_cols.map(&:name).each {|name| row(name) {|i| JSON.pretty_unparse i[name]}}
      end
    end
    
    panel "Actigraphy records by date" do
      table_for user.actigraphies do |t|
        t.column(:date) {|i| auto_link i, i.date}
        t.column(:aggregates) {|i| JSON.pretty_unparse i.aggregates}
        t.column(:metrics) {|i| JSON.pretty_unparse i.metrics}
        t.column(:workouts) {|i| JSON.pretty_unparse i.workouts}
      end
    end
    
    panel "Body mass records" do
      table_for user.bodymasses do |t|
        t.column(:datetime_utc) {|i| auto_link i, i.datetime_utc}
        t.column(:fat_mass)
        t.column(:lean_mass)
        t.column(:mass)
        t.column(:bmi)
        t.column(:fat_percent)
      end
    end
    
    panel "Data sources" do
      table_for user.data_sources do |t|
        t.column(:name) {|i| auto_link i, i.name}
        t.column(:active)
        t.column(:advertised_name)
        t.column(:serial_number)
        t.column(:_embedded) {|i| JSON.pretty_unparse i._embedded}
      end
    end
  end

end
