ActiveAdmin.register Underarmour::Bodymass do

  index do
    selectable_column
    
    column(:external_id) {|i| auto_link i, i.external_id}
    column(:datetime_utc)
    column(:user) {|i| auto_link i.user}
    column(:fat_mass)
    column(:lean_mass)
    column(:mass)
    column(:bmi)
    column(:fat_percent)
    
    actions
  end

  filter :user
  
  show title: ->(x) {"#{x.user.display_name} body mass records"} do |x|
    panel "Details" do
      attributes_table_for x do
        row(:external_id) {|i| auto_link i, i.external_id}
        row(:datetime_utc)
        row(:user) {|i| auto_link i.user}
        row(:fat_mass)
        row(:lean_mass)
        row(:mass)
        row(:bmi)
        row(:fat_percent)
        row(:datetime_timezone)
        row(:updated_datetime)
        row(:updated_datetime)
      end
    end
  end

end
