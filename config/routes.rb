Rails.application.routes.draw do
  devise_config = ActiveAdmin::Devise.config
  devise_config[:controllers][:omniauth_callbacks] = 'users/omniauth_callbacks'
  
  devise_for :users, devise_config
  
  ActiveAdmin.routes(self)
  
  root 'templates#dashboard'
  
  get '/templates/*id' => 'templates#show', as: :template, format: false
end