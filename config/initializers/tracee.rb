# Defines logger with the formatter suitable to trace-debug an application
  $log = Tracee::Logger.new\
    streams: [$stdout, {cascade: Rails.root.join('log', "#{Rails.env}.%{level}.log").to_s}],
    formatter: {:formatter => :tracee}