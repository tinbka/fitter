ENV['RACK_ENV'] ||= 'development'
ENV['PORT']     ||= 3000

# 1 worker limit is needed at development for better_errors inspection to work
workers Integer(ENV['WEB_CONCURRENCY'] || (ENV['RACK_ENV'] == 'development' ? 1 : 4))
threads 1, Integer(ENV['MAX_THREADS'] || 1)

preload_app!

rackup      DefaultRackup
port        Integer(ENV['PORT'])
environment ENV['RACK_ENV']
# Otherwise it will change to OS root
directory   File.expand_path('../../', __FILE__)

on_worker_boot do
  # Worker specific setup for Rails 4.1+
  # See: https://devcenter.heroku.com/articles/deploying-rails-applications-with-the-puma-web-server#on-worker-boot
  ActiveRecord::Base.establish_connection
end