lock '3.4.0'

set :application, 'fitter'
#set :repo_url, 'file:///home/shinku/apps/fitter/.git'
#set :scm, :git_copy
set :repo_url, 'git@bitbucket.org:tinbka/fitter.git'
set :scm, :git

set :stages, %w(production staging)

# Default branch is :master
if ENV['branch']
  set :branch, ENV['branch'] || 'master'
end

# Don't change these unless you know what you're doing
set :pty,             true
set :use_sudo,        false
set :deploy_to,       "/home/nitrous/code/#{fetch(:application)}"

set :puma_bind,       "0.0.0.0:80"
set :puma_state,      "#{shared_path}/tmp/pids/puma.state"
set :puma_pid,        "#{shared_path}/tmp/pids/puma.pid"
set :puma_access_log, "#{release_path}/log/puma.error.log"
set :puma_error_log,  "#{release_path}/log/puma.access.log"
set :puma_preload_app, true
set :puma_worker_timeout, nil
set :puma_init_active_record, true  # Change to false when not using ActiveRecord
set :puma_threads,    [4, 16]
set :puma_workers,    0

# Default value for :format is :pretty
# set :format, :pretty

# Default value for :log_level is :debug
# set :log_level, :debug

# Default value for :linked_files is []
set :linked_files,  %w{config/database.yml .env config/settings.local.yml}

# Default value for linked_dirs is []
set :linked_dirs,   %w{bin log tmp/pids tmp/cache tmp/sockets public/assets}

set :rvm1_ruby_version, '2.2.3'
set :rvm_ruby_version, '2.2.3'

# Default value for keep_releases is 5
set :keep_releases, 10

#set :bundle_jobs, 4
set :bundle_without, %w{development test}.join(' ')
set :bundle_binstubs, -> { shared_path.join('bin') }

# PUMA
# --------------------------------------------
namespace :puma do
  desc 'Create Directories for Puma Pids and Socket'
  task :make_dirs do
    on roles(:app) do
      execute "mkdir #{shared_path}/tmp/sockets -p"
      execute "mkdir #{shared_path}/tmp/pids -p"
    end
  end
  
  task :link_config do
    on roles(:app) do
      execute "ln -nfs #{shared_path}/puma.rb #{release_path}/config/puma.rb"
    end
  end

  before :start, :make_dirs
  before :start, :link_config
  before :restart, :link_config
end

namespace :deploy do
  desc "Make sure local git is in sync with remote."
  task :check_revision do
    on roles(:app) do
      unless `git rev-parse HEAD` == `git rev-parse origin/master`
        puts "WARNING: HEAD is not the same as origin/master"
        puts "Run `git push` to sync changes."
        exit
      end
    end
  end

  desc 'Initial Deploy'
  task :initial do
    on roles(:app) do
      before 'deploy:restart', 'puma:start'
      invoke 'deploy'
    end
  end

  desc 'Restart application'
  task :restart do
    on roles(:app), in: :sequence, wait: 5 do
      invoke 'puma:restart'
    end
  end
  
  #before :starting,     :check_revision
  after  :finishing,    :compile_assets
  after  :finishing,    :cleanup
  after  :finishing,    :restart
end

# ps aux | grep puma    # Get puma pid
# kill -s SIGUSR2 pid   # Restart puma
# kill -s SIGTERM pid   # Stop puma
